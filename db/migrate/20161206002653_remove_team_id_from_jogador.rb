class RemoveTeamIdFromJogador < ActiveRecord::Migration[5.0]
  def change
    remove_column :jogadors, :team_id, :integer
  end
end
