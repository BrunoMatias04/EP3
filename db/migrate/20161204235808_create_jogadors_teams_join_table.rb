class CreateJogadorsTeamsJoinTable < ActiveRecord::Migration[5.0]
  def change
    create_join_table :jogadors, :teams do |t|
      t.index [:jogador_id, :team_id]
      t.index [:team_id, :jogador_id]
    end
  end
end
