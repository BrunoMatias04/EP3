class CreateTorneios < ActiveRecord::Migration[5.0]
  def change
    create_table :torneios do |t|
      t.string :Nome
      t.date :Inicio
      t.date :Fim

      t.timestamps
    end
  end
end
