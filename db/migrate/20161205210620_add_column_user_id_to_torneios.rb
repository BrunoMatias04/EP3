class AddColumnUserIdToTorneios < ActiveRecord::Migration[5.0]
  def change
    add_column :torneios, :user_id, :integer
  end
end
