class CreateJogadors < ActiveRecord::Migration[5.0]
  def change
    create_table :jogadors do |t|
      t.string :Nome
      t.string :Nick_no_jogo
      t.integer :Idade

      t.timestamps
    end
  end
end
