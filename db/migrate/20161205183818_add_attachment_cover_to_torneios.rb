class AddAttachmentCoverToTorneios < ActiveRecord::Migration
  def self.up
    change_table :torneios do |t|
      t.attachment :cover
    end
  end

  def self.down
    remove_attachment :torneios, :cover
  end
end
