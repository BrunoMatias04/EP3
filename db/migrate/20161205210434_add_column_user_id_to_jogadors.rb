class AddColumnUserIdToJogadors < ActiveRecord::Migration[5.0]
  def change
    add_column :jogadors, :user_id, :integer
  end
end
