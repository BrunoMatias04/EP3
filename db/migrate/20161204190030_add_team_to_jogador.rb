class AddTeamToJogador < ActiveRecord::Migration[5.0]
  def change
    add_reference :jogadors, :team, foreign_key: true
  end
end
