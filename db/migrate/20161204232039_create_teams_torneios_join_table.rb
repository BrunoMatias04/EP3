class CreateTeamsTorneiosJoinTable < ActiveRecord::Migration[5.0]
  def change
    create_table :teams_torneios, id: false do |t|
      t.integer :team_id
      t.integer :torneio_id
    end
    add_index :teams_torneios, :team_id
    add_index :teams_torneios, :torneio_id
  end
end
