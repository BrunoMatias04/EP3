# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161206002653) do

  create_table "jogadors", force: :cascade do |t|
    t.string   "Nome"
    t.string   "Nick_no_jogo"
    t.integer  "Idade"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "user_id"
  end

  create_table "jogadors_teams", id: false, force: :cascade do |t|
    t.integer "jogador_id", null: false
    t.integer "team_id",    null: false
    t.index ["jogador_id", "team_id"], name: "index_jogadors_teams_on_jogador_id_and_team_id"
    t.index ["team_id", "jogador_id"], name: "index_jogadors_teams_on_team_id_and_jogador_id"
  end

  create_table "teams", force: :cascade do |t|
    t.string   "Nome"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.integer  "user_id"
  end

  create_table "teams_torneios", id: false, force: :cascade do |t|
    t.integer "team_id"
    t.integer "torneio_id"
    t.index ["team_id"], name: "index_teams_torneios_on_team_id"
    t.index ["torneio_id"], name: "index_teams_torneios_on_torneio_id"
  end

  create_table "torneios", force: :cascade do |t|
    t.string   "Nome"
    t.date     "Inicio"
    t.date     "Fim"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "cover_file_name"
    t.string   "cover_content_type"
    t.integer  "cover_file_size"
    t.datetime "cover_updated_at"
    t.integer  "user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
