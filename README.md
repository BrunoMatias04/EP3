# EP2 OO-2016.2(UnB Gama)
Esse projeto consiste em uma aplicação web , feita em ambiente ruby on rails , com funcionalidades para controle de torneio de jogos eletronicos.

-UML disponivel na pasta do projeto e no relatório.

# Requisitos
- Ruby 2.~
- Ruby on rails 5~
- Gems necessárias: Dispónivel em Gems.file



# Para Executar o programa
- Primeiramente deve se baixar o zip no repositorio do projeto ou dar um git clone com o link do repositorio.
- Utilizar o prompt do linux , contendo o rails.
- Abrir pelo prompt o diretorio em que o projeto foi salvo.
- Utilizar o comando "bundle install" e logo após instalar a gem bootstrap separado utlizando o comando "gem install bootstrap_form -v '2.5.2'
" e por fim utilizar o comando "rake db:migrate".
- Utilizar o comando "rails s" na prompt aberta no diretorio do projeto para inicializar o servidor.
- Por fim abrir um browser de preferencia do usuario e entrar em tcp://localhost:3000.

# Abrindo o programa ... 
- Irá se deparar com uma pagina de verificação de cadastro do usuario , onde o usuario deve primeiramente se cadastrar e logar para finalmente entrar na pagina inicial.
- Na pagina inicial , que contem todas as opções de controle de informações referentes a jogador , time e torneio , onde o usuario poderá criar cadastro e modificar informaçõs
- Para cada opção de cadastro existe suas validações , em que o usuario irá se peparar , como campos obrigatórios a serem preenchidos.
- Nas opções de "mostrar" de cada cadastro , irá aparecer seus atributos , e principalmente os atributos atrelados as models , como um jogador e seu(s) time(s).
- Nas opções de "editar" para cada cadastro referente a sua model , o usuário terá a possibilidade de mudar qualquer informação.
- Nas opções de "destruir" referente a cada tipo de cadastro , o usuário terá como destruir um cadastro. 