class TorneiosController < ApplicationController
  before_action :set_torneio, only: [:show, :edit, :update, :destroy]

  # GET /torneios
  # GET /torneios.json
  def index
    @torneios = Torneio.where("user_id = "+current_user.id.to_s)
  end

  # GET /torneios/1
  # GET /torneios/1.json
  def show
  end

  # GET /torneios/new
  def new
    @torneio = Torneio.new
  end

  # GET /torneios/1/edit
  def edit
  end

  # POST /torneios
  # POST /torneios.json
  def create
    @torneio = Torneio.new(torneio_params)
    @torneio.user_id = current_user.id if current_user

    respond_to do |format|
      if @torneio.save
        format.html { redirect_to @torneio, notice: 'Torneio was successfully created.' }
        format.json { render :show, status: :created, location: @torneio }
      else
        format.html { render :new }
        format.json { render json: @torneio.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /torneios/1
  # PATCH/PUT /torneios/1.json
  def update
    @torneio.user_id = current_user.id if current_user
    respond_to do |format|
      if @torneio.update(torneio_params)
        format.html { redirect_to @torneio, notice: 'Torneio was successfully updated.' }
        format.json { render :show, status: :ok, location: @torneio }
      else
        format.html { render :edit }
        format.json { render json: @torneio.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /torneios/1
  # DELETE /torneios/1.json
  def destroy
    @torneio.destroy
    respond_to do |format|
      format.html { redirect_to torneios_url, notice: 'Torneio was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_torneio
      @torneio = Torneio.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def torneio_params
      params.require(:torneio).permit(:cover , :user_id, :Nome, :Inicio, :Fim ,:team_ids => [])
    end
end
