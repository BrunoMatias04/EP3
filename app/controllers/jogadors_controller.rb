class JogadorsController < ApplicationController
  before_action :set_jogador, only: [:show, :edit, :update, :destroy]

  # GET /jogadors
  # GET /jogadors.json
  def index
    @jogadors = Jogador.where("user_id ="+current_user.id.to_s)
  end

  # GET /jogadors/1
  # GET /jogadors/1.json
  def show
  end

  # GET /jogadors/new
  def new
    @jogador = Jogador.new
  end

  # GET /jogadors/1/edit
  def edit
  end

  # POST /jogadors
  # POST /jogadors.json
  def create
    @jogador = Jogador.new(jogador_params)
    @jogador.user_id = current_user.id if current_user

    respond_to do |format|
      if @jogador.save
        format.html { redirect_to @jogador, notice: 'Jogador was successfully created.' }
        format.json { render :show, status: :created, location: @jogador }
      else
        format.html { render :new }
        format.json { render json: @jogador.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /jogadors/1
  # PATCH/PUT /jogadors/1.json
  def update
    @jogador.user_id = current_user.id if current_user
    respond_to do |format|

      if @jogador.update(jogador_params)
        format.html { redirect_to @jogador, notice: 'Jogador was successfully updated.' }
        format.json { render :show, status: :ok, location: @jogador }
      else
        format.html { render :edit }
        format.json { render json: @jogador.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /jogadors/1
  # DELETE /jogadors/1.json
  def destroy
    @jogador.destroy
    respond_to do |format|
      format.html { redirect_to jogadors_url, notice: 'Jogador was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_jogador
      @jogador = Jogador.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def jogador_params
      params.require(:jogador).permit(:Nome, :Nick_no_jogo, :user_id, :Idade , :team_ids => [])
    end
end
