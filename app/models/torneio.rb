class Torneio < ApplicationRecord
  has_and_belongs_to_many :teams
  belongs_to :user
  has_attached_file :cover ,styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/assets/image.png"
  validates_attachment :cover, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"] }
  validates_presence_of :Nome, message: "Preenchimento obrigatório"
  validates_uniqueness_of :Nome, message: "já existente"
end
