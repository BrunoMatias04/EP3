class Jogador < ApplicationRecord
  has_and_belongs_to_many  :team
  belongs_to :user
  validates_presence_of :Nome, message: "Preenchimento obrigatório"
  validates_presence_of :Nick_no_jogo, message: "Preenchimento obrigatório"
  validates_presence_of :Idade, message: "Preenchimento obrigatório"
  validates_uniqueness_of :Nome, message: "já cadastrado"
  validates_uniqueness_of :Nick_no_jogo, message: "já utilizado"



end
