class Team < ApplicationRecord
  has_and_belongs_to_many  :jogadors
  has_and_belongs_to_many :torneios
  belongs_to :user
  has_attached_file :logo ,styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/assets/escudo.jpg"
  validates_attachment :logo, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"] }
  validates_presence_of :Nome, message: "Preenchimento obrigatório"
  validates_uniqueness_of :Nome, message: "já existente"
end
